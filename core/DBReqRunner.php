<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/10
 * Time: 13:35
 */

namespace sinri\dbreq\core;


use sinri\dbreq\entity\IssueEntity;
use sinri\dbreq\model\IssueModel;

class DBReqRunner
{
    public function run()
    {
        if (php_sapi_name() != 'cli') {
            DBReq()->loggerForCli()->error('You must run this in CLI mode!');
            return;
        }
        $this->executeRecentOne();
    }

    protected function executeRecentOne()
    {
        try {
            // 找出最近待执行的一条 用 for update 锁住 更新为执行中
            $issue_id = $this->seekOneIssueToExecute($seek_error);

            // 判断有没有成功
            if (empty($issue_id)) {
                if ($seek_error !== "ISSUE QUEUE EMPTY") {
                    DBReq()->loggerForCli()->error("seekOneIssueToExecute failed: {$seek_error}");
                }
                return false;
            }
            DBReq()->loggerForCli()->info("seekOneIssueToExecute got: {$issue_id}");
            // 获取请求和数据库信息
            $issue = new IssueEntity($issue_id);
            if (!$issue) {
                DBReq()->loggerForCli()->error("getIssueInfoForExecuting failed: {$issue_id}");
                return false;
            }
            DBReq()->loggerForCli()->info("getIssueInfoForExecuting got", [
                $issue->getDatabaseMeta()->nick,
                $issue->getDatabaseMeta()->id
            ]);
            if ($issue->getIssueMeta()->status !== IssueModel::STATUS_RUNNING) {
                DBReq()->loggerForCli()->error("The issue[{$issue_id}] is not in status of STATUS_RUNNING");
            }

            $issue->execute();
            return true;
        } catch (\Exception $exception) {
            DBReq()->loggerForCli()->error("executeRecentOne Exception: " . $exception->getMessage());
            return false;
        }
    }

    protected function seekOneIssueToExecute(&$error = null)
    {
        $error = "";
        try {
            DBReq()->db()->beginTransaction();

            $value_of_STATUS_QUEUED = IssueModel::STATUS_QUEUED;
            $value_of_STATUS_RUNNING = IssueModel::STATUS_RUNNING;

            // IS IT NEEDED TO CHECK RUNNING ISSUE?
            // -> NOT NOW

            // SEEK QUEUED ISSUE
            $SQL_GET_OLDEST = "SELECT id from req_issue
                where status = {$value_of_STATUS_QUEUED} 
                order by update_time limit 1 for update
            ";
            $target_issue_id = DBReq()->db()->getOne($SQL_GET_OLDEST);
            $target_issue_id = intval($target_issue_id, 10);
            if (empty($target_issue_id)) {
                throw new \Exception("ISSUE QUEUE EMPTY", 1);
            }

            // MAKE IT LOCKED
            $SQL_MARK_ISSUE = "UPDATE req_issue
                set status = {$value_of_STATUS_RUNNING} 
                where status = {$value_of_STATUS_QUEUED} and id = " . ($target_issue_id);
            $afx = DBReq()->db()->exec($SQL_MARK_ISSUE);
            if ($afx !== 1) {
                throw new \Exception("ISSUE LOCK FAILED", 1);
            }

            DBReq()->db()->commit();

            return ($target_issue_id);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            try {
                DBReq()->db()->rollBack();
            } catch (\Exception $e) {
                //who cares
            }
            return false;
        }
    }

}