<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/9
 * Time: 13:38
 */

namespace sinri\dbreq\core;


use sinri\ark\core\ArkHelper;
use sinri\ark\database\mysql\ArkMySQLi;
use sinri\ark\database\mysql\ArkMySQLiConfig;
use sinri\dbreq\model\HostModel;
use sinri\dbreq\model\QuickQueryModel;

class DatabaseAdapter
{
    /**
     * @var HostModel
     */
    protected $databaseMeta;

    /**
     * @var \mysqli
     */
    protected $mysqli;

    /**
     * DatabaseAdapter constructor.
     * @param int $databaseId
     * @throws \Exception
     */
    public function __construct($databaseId)
    {
        $this->databaseMeta = new HostModel();
        $row = $this->databaseMeta->selectRow(['id' => $databaseId]);
        ArkHelper::assertItem($row, 'no such target db');
        $this->databaseMeta->loadFieldsFromRowArray($row);

        $this->getMySQLiInstance();

//        if (!DBReq()->getCurrentSession()->isAsAdmin()) {
//            $permittedDatabaseList = DBReq()->getCurrentSession()->getUser()->getPermittedDatabasesForAuthType(DBReqCore::TYPE_QUERY);
//            if (!in_array($databaseId, $permittedDatabaseList)) {
//                ArkHelper::assertItem("not permitted");
//            }
//        }
    }

    /**
     * 执行SQL返回数据
     * @param string $sql
     * @param array $debug
     * @return array
     * @throws \Exception
     */
    public function runQuickQuery($sql, &$debug = [])
    {
        $debug = [];
        // this is very important to avoid go into the bug of Parser
        $first_sql = $sql . PHP_EOL . ";";

        // validateSql now no limitation on sql
        $correct = $this->validateSql($first_sql, DBReqCore::TYPE_SELECT, $error_list);
        if (!$correct) {
            throw new \Exception("SQL查询语句有妖气: " . implode(';', $error_list));
        }

        $splitter = new SQLSplitter();
        $real_sql = $splitter->processSqlForQuickQuery($first_sql, 512);
        $debug['real_sql'] = $real_sql;

        DBReq()->loggerForWeb()->info(__METHOD__ . ' BEFORE: ' . $sql);
        DBReq()->loggerForWeb()->info(__METHOD__ . ' AFTER: ' . $real_sql);

        $query_id = (new QuickQueryModel())->recordQuickQuery(
            DBReq()->getCurrentSession()->getUser()->getUserId(),
            $this->databaseMeta->id,
            $sql
        );
        ArkHelper::assertItem($query_id, "Cannot record");

        $query_start_time = microtime(true);

        $done = $this->quickQueryWithMySQLi($real_sql, $result, $error, 512);

        $query_end_time = microtime(true);

        $time_cost = ($query_end_time - $query_start_time);
        $debug['duration'] = $time_cost;

        if ($done === false) {
            throw new \Exception(implode(PHP_EOL, $error));
        }

        (new QuickQueryModel())->feedbackQueryQueryDuration($query_id, $time_cost);

        return $result;
    }

    /**
     * @param string $query
     * @param string $type IssueModel::TYPE_*
     * @param null|string[] $error
     * @return bool
     */
    public function validateSql($query, $type, &$error = null)
    {
        $splitter = new SQLSplitter();
        $checker = new SQLChecker();

        $error = array();
        $validated = true;

        // SPECIAL HANDLER FOR CALL STATEMENT
        if ($checker->checkIfAvailableCallStatement($query) && $type == DBReqCore::TYPE_STRUCTURE) {
            return true;
        }
        if ($checker->checkIfAvailableTruncateStatement($query) && $type == DBReqCore::TYPE_STRUCTURE) {
            return true;
        }

        $list = $splitter->split($query);
        $list = array_filter($list);
        if (empty($list)) {
            $error = array('提取不出支持的SQL喵。' . 'from query input: ' . $query);
            return false;
        }
        foreach ($list as $index => $sql) {
            $correct = $checker->validateSqlGrammar($sql, $syntax_error);
            if ($correct) {
                $analyzed_type = $splitter->getTypeOfSingleSql($sql);
                if (in_array($analyzed_type, array('UPDATE', 'INSERT', 'DELETE', 'REPLACE'))
                    && $type == DBReqCore::TYPE_UPDATE) {
                    //OK
                } elseif (in_array($analyzed_type, array('SELECT', 'SHOW'))
                    && $type == DBReqCore::TYPE_SELECT) {
                    //OK
                } elseif (in_array($analyzed_type, array('ALTER', 'CREATE', 'DROP', 'CALL'))
                    && $type == DBReqCore::TYPE_STRUCTURE) {
                    //OK
                } elseif (in_array($analyzed_type, array('EXPLAIN'))
                    && $type == DBReqCore::TYPE_SELECT) {
                    //OK
                } else {
                    //array('ANALYZE','CHECK','CHECKSUM','OPTIMIZE','REPAIR','CALL','EXPLAIN')
                    $validated = false;
                    $error[] = "第" . ($index + 1) . "条SQL的声明类型({$type})"
                        . "和分析结论({$analyzed_type})不符合。";
                }
            } else {
                $validated = false;
                $error[] = "第" . ($index + 1) . "条SQL的语法有问题：" . $syntax_error;
            }
        }
        return $validated;
    }

    /**
     * @param string $query
     * @param array $data
     * @param null|string[] $error
     * @param int $limit
     * @return bool
     */
    protected function quickQueryWithMySQLi($query, &$data = [], &$error = null, $limit = 512)
    {
        $error = [];
        $data = [];

        //set execute timeout as 10 seconds
        //$this->mysqli->options(11 /*MYSQLI_OPT_READ_TIMEOUT*/, 10);

        if (!($this->mysqli->multi_query($query))) {
            $error[] = $this->mysqli->error;
            return false;
        }
        $result = $this->mysqli->store_result();
        if (!$result) {
            $error[] = $this->mysqli->error;
            return false;
        }

        DBReq()->loggerForWeb()->info(__METHOD__ . ' raw quick query result', ["result" => $result]);
        if ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            do {
                if ($limit > 0 && $result->num_rows >= $limit) {
                    $error[] = "SQL返回行数超过上限（{$limit}）！";
                    break;
                }
                $data[] = $row;
            } while ($row = $result->fetch_array(MYSQLI_ASSOC));
        }
        $result->free();
        return true;
    }

    /**
     * @param string $query
     * @param string[] $error
     * @param string $charset target file charset
     * @return bool|string Temp CSV File Path
     */
    public function queryAndExportWithMySQLi($query, &$error, $charset = 'gbk')
    {
        $error = array();

        $csv_path = tempnam(sys_get_temp_dir(), 'DBReq');
        chmod($csv_path, 0655);
        $csv_file = fopen($csv_path, 'w');
        // $mysqli = $this->_connect($db);

        if (!($this->mysqli->multi_query($query))) {
            $error[] = $this->mysqli->error;
            return false;
        }
        $result = $this->mysqli->store_result();
        if (!$result) {
            $error[] = $this->mysqli->error;
            return false;
        }

        if ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            fputcsv($csv_file, array_keys($row));
            do {
                //array_walk($row, 'self::transCharset', array('utf8', $charset));
                foreach ($row as &$item) {
                    $item = mb_convert_encoding($item, $charset, 'utf8');
                }
                fputcsv($csv_file, array_values($row));
            } while ($row = $result->fetch_array(MYSQLI_ASSOC));
        }
        $result->free();
        //$this->mysqli->close();
        return $csv_path;
    }

    public function executeCall($query, &$error)
    {
        $error = array();
        if (!$this->mysqli->query($query)) {
            $error['1'] = "CALL failed: (" . $this->mysqli->errno . ") " . $this->mysqli->error;
            return false;
        }
        return true;
    }

    /**
     * @param string $query
     * @param int $type
     * @param int[] $affected
     * @param string[] $error
     * @return bool
     */
    public function executeMulti($query, $type, &$affected, &$error)
    {
        $affected = array();
        $error = array();

        // $mysqli = $this->_connect($db);

        // 开启一个事务
        // 保证中途任何语句发生错误都完全回滚
        // note: 如果表的engine不是INNODB，无法回滚
        $this->mysqli->autocommit(false);

        try {
            $sqlIdx = 1;
            if ($this->mysqli->multi_query($query)) {
                do {
                    $affected[] = $this->mysqli->affected_rows;

//                if ($type==IssueModel::TYPE_UPDATE && $this->mysqli->affected_rows <= 0) {
//                    $error[$sqlIdx] = 'This statement has no effect';
//                    $this->mysqli->rollback();
//                    $this->mysqli->close();
//                    return false;
//                }
                    $sqlIdx++;

                    if ($type == DBReqCore::SPECIAL_TYPE_CALL_STATEMENT) {
                        break;
                    }
                } while ($this->mysqli->more_results() && $this->mysqli->next_result() && !$this->mysqli->errno);
            } else {
                throw new \Exception($this->mysqli->error);
            }

            if ($this->mysqli->errno) {
                $error[$sqlIdx] = $this->mysqli->error;
                throw new \Exception($this->mysqli->error);
            }

            $this->mysqli->commit();
            //$this->mysqli->close();
            return true;
        } catch (\Exception $exception) {
            $error[0] = $exception->getMessage();
            $this->mysqli->rollback();
            return false;
        }
    }


    /**
     * @throws \Exception
     */
    protected function getMySQLiInstance()
    {
        $config = (new ArkMySQLiConfig())
            ->setHost($this->databaseMeta->addr)
            ->setPort($this->databaseMeta->port)
            ->setUsername($this->databaseMeta->user)
            ->setPassword(base64_decode($this->databaseMeta->pass));
        $arkMySQLi = (new ArkMySQLi($config));
        $arkMySQLi->connect();
        $this->mysqli = $arkMySQLi->getInstanceOfMySQLi();
        //var_dump($this->mysqli);
    }


}