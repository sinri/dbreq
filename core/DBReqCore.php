<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:15
 */

namespace sinri\dbreq\core;


use sinri\ark\core\ArkHelper;
use sinri\ark\core\ArkLogger;
use sinri\ark\database\pdo\ArkPDO;
use sinri\ark\database\pdo\ArkPDOConfig;
use sinri\dbreq\entity\SessionEntity;
use sinri\dbreq\plugin\interfaces\UserPlugin;

class DBReqCore
{
    const TYPE_QUERY = -1;//快速查询: -1
    const TYPE_UPDATE = 0;//更新请求: 0
    const TYPE_SELECT = 1;//导出请求: 1
    const TYPE_STRUCTURE = 2;//结构请求: 2
    const SPECIAL_TYPE_CALL_STATEMENT = 3;//专门搞一个类型给CALL

    private static $instance;
    /**
     * @var SessionEntity
     */
    protected $currentSession;
    /**
     * @var UserPlugin
     */
    protected $userPlugin;
    private $webLogger;
    private $cliLogger;
    private $pdo;

    protected function __construct()
    {
    }

    /**
     * @return DBReqCore
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new DBReqCore();
        }
        return self::$instance;
    }

    /**
     * @return SessionEntity
     */
    public function getCurrentSession(): SessionEntity
    {
        return $this->currentSession;
    }

    /**
     * @param SessionEntity $currentSession
     */
    public function setCurrentSession(SessionEntity $currentSession)
    {
        $this->currentSession = $currentSession;
    }

    /**
     * @return ArkLogger
     */
    public function loggerForWeb()
    {
        if (!$this->webLogger) {
            $this->webLogger = new ArkLogger($this->readConfig(['log_dir'], __DIR__ . '/../runtime/log'), 'web');
        }
        return $this->webLogger;
    }

    /**
     * @param $keychain
     * @param null $default
     * @param string $aspect
     * @return mixed|null
     */
    public function readConfig($keychain, $default = null, $aspect = "config")
    {
        $config_file = __DIR__ . '/../config/' . $aspect . ".php";
        if (!file_exists($config_file)) {
            return $default;
        }
        $config = [];
        /** @noinspection PhpIncludeInspection */
        require $config_file;
        return ArkHelper::readTarget($config, $keychain, $default);
    }

    /**
     * @return ArkLogger
     */
    public function loggerForCli()
    {
        if (!$this->cliLogger) {
            $this->cliLogger = new ArkLogger($this->readConfig(['log_dir'], __DIR__ . '/../runtime/log'), 'cli');
        }
        return $this->cliLogger;
    }

    /**
     * @return ArkPDO
     * @throws \Exception
     */
    public function db()
    {
        if (!$this->pdo) {
            $this->pdo = new ArkPDO();
            $this->pdo->setPdoConfig(new ArkPDOConfig($this->readConfig(['database'])));
            $this->pdo->connect();
        }
        return $this->pdo;
    }

    /**
     * @return UserPlugin
     */
    public function getUserPlugin()
    {
        if (!$this->userPlugin) {
            $this->userPlugin = UserPlugin::loadInstance();
        }
        return $this->userPlugin;
    }
}