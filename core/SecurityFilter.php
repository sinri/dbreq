<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 15:51
 */

namespace sinri\dbreq\core;


use sinri\ark\core\ArkHelper;
use sinri\ark\web\ArkRequestFilter;
use sinri\dbreq\entity\SessionEntity;


class SecurityFilter extends ArkRequestFilter
{

    /**
     * Check request data with $_REQUEST, $_SESSION, $_SERVER, etc.
     * And decide if the request should be accepted.
     * If return false, the request would be thrown.
     * You can pass anything into $preparedData, that controller might use it (not sure, by the realization)
     * @param $path
     * @param $method
     * @param $params
     * @param mixed $preparedData
     * @param int $responseCode
     * @param null|string $error
     * @return bool
     */
    public function shouldAcceptRequest($path, $method, $params, &$preparedData = null, &$responseCode = 200, &$error = null)
    {
        $responseCode = 200;
        try {
            if ($path === '/api/Security/apiRequestToken') {
                return true;
            }
            if (self::hasPrefixAmong($path, ['/api/SiteInfo/'])) {
                return true;
            }

            $token = Ark()->webInput()->readRequest("token");
            ArkHelper::assertItem($token, "token lack, as " . json_encode($token));
            $session = new SessionEntity($token);
            $session->loadSession();
            DBReq()->setCurrentSession($session);

            return true;
        } catch (\Exception $exception) {
            $responseCode = 403;
            $error = $exception->getMessage();
            return false;
        }
    }

    /**
     * Give filter a name for Error Report
     * @return string
     */
    public function filterTitle()
    {
        return "DBREQ-SECURITY-FILTER";
    }
}