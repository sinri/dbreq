<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/9
 * Time: 13:50
 */

namespace sinri\dbreq\core;


use PhpMyAdmin\SqlParser\Components\Limit;
use PhpMyAdmin\SqlParser\Parser;
use PhpMyAdmin\SqlParser\Statement;
use PhpMyAdmin\SqlParser\Utils\Query;

class SQLSplitter
{
    public function getTypeOfSingleSql($query)
    {
        $parser = new Parser($query);
        $flags = Query::getFlags($parser->statements[0]);
        return $flags['querytype'];
    }

    /**
     * @param string $sql
     * @param int $max_limit
     * @return string The original sql if well limited, or the modified sql.
     */
    public function processSqlForQuickQuery($sql, $max_limit = 512)
    {
        $split_result = $this->split($sql);
        $sql = $split_result[0];
        $parser = new Parser($sql);
        if (
            !isset($parser->statements[0]->limit)
            || !$parser->statements[0]->limit
            || $parser->statements[0]->limit->rowCount > $max_limit
        ) {
            $parser->statements[0]->limit = new Limit(30, 0);
            $statement = $parser->statements[0];
            $sql2 = $statement->build();
            return $sql2;
        }
        return $sql;
    }

    public function split($query)
    {
        $parser = new Parser($query);
        // $flags = Query::getFlags($parser->statements[0]);
        // return $flags;
        $result = array();
        $statements = $parser->statements;
        if (!is_array($statements)) {
            $statements = [$statements];
        }
        foreach ($statements as $statement) {
            $single_sql = $this->dealStatement($statement);
            $result[] = $single_sql;
        }
        return $result;
    }

    /**
     * @param Statement $statement
     * @return string
     */
    private function dealStatement($statement)
    {
        $sql = $statement->build();
        return $sql;
    }
}