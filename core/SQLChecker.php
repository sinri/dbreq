<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/9
 * Time: 13:49
 */

namespace sinri\dbreq\core;


use PHPSQLParser\PHPSQLParser;

class SQLChecker
{
    public function validateSqlGrammar($sql, &$error = null)
    {
        try {
            // Try to fix 'cannot calculate position' issue, turn it off
            $calcPositions = false;
            $parser = new PHPSQLParser($sql, $calcPositions);
            return ($parser->parsed != false);
        } catch (\Exception $e) {
            $error = $e->getMessage();
            return false;
        }
    }

    public function checkIfAvailableCallStatement($sql)
    {
        $x_sql = trim($sql);
        $x_sql = preg_replace('/^--[\s\r\n]?.*$/', '', $x_sql);
        $x_sql = preg_replace('/[\r\n]+/', '', $x_sql);
        if (stripos($x_sql, 'call ') === 0) {
            //这货是CALL
            if (preg_match('/^call +[A-Za-z0-9_]+\((((\'.+\')|([0-9.]+))?(,((\'.+\')|([0-9.]+)))*)\);?$/', $x_sql)) {
                //符合语法
                return true;
            }
        }
        return false;
    }

    public function checkIfAvailableTruncateStatement($sql)
    {
        $x_sql = trim($sql);
        //$x_sql=preg_replace('/^--[\s\r\n]?.*$/', '', $x_sql);
        if (preg_match(
            '/^(truncate ([A-Za-z0-9_]+\.)[A-Za-z0-9_]+)(\s*;\s*(truncate ([A-Za-z0-9_]+\.)[A-Za-z0-9_]+)?)*$/',
            $x_sql
        )) {
            // 这货是truncate
            return true;
        }
        return false;
    }
}