<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/16
 * Time: 09:58
 */

use sinri\dbreq\core\DBReqRunner;

require_once __DIR__ . '/autoload.php';
date_default_timezone_set("Asia/Shanghai");

(new DBReqRunner())->run();