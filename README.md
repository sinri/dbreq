# DBREQ - Open Source Version

Powered by Project Ark (Version 0.10).

## Deploy

1. Prepare server and database. DBREQ requires Linux, PHP 7 (FPM/CGI and CLI), Apache or Nginx, MySQL, etc.
1. Initialize MySQL user account and scheme, create tables as `model/README.md` suggests.
1. Fetch codes (by cloning the git repository or downloading) and put onto server.
1. Run `composer install` to get required packages.
1. Run `php initAdmin.php` to initialize the admin user.
1. Write the configuration files in `config`, following the sample files.
1. Configure web servers, write `.htaccess` for Apache or config file for Nginx, to make all non-file-targeted requests to `index.php`.

Now try!