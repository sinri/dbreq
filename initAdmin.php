<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/16
 * Time: 13:28
 */

require_once __DIR__ . '/autoload.php';
date_default_timezone_set("Asia/Shanghai");

if (!\sinri\ark\core\ArkHelper::isCLI()) {
    echo "CLI..." . PHP_EOL;
    exit;
}

$existed = (new \sinri\dbreq\model\UserModel())->selectRowsForCount(['is_admin' => 1]);
if ($existed) {
    echo "ADMIN existed!" . PHP_EOL;
    exit;
}

$pw = uniqid();
echo "Going to create an admin account..." . PHP_EOL;

$user_id = (new \sinri\dbreq\model\UserModel())->insert([
    'user_id' => -1,
    'is_admin' => 1,
    'user_name' => 'admin',
    'real_name' => 'Administrator',
    'password' => password_hash($pw, PASSWORD_DEFAULT),
]);

if ($user_id == -1) {
    echo "Success, username is admin and password is " . $pw . PHP_EOL;
} else {
    echo "Failed" . PHP_EOL;
}