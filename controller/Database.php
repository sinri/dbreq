<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 15:41
 */

namespace sinri\dbreq\controller;


use sinri\ark\core\ArkHelper;
use sinri\ark\web\implement\ArkWebController;
use sinri\dbreq\model\HostModel;

class Database extends ArkWebController
{
    /**
     * 更新数据库信息
     */
    public function apiUpdateDatabase()
    {
        try {
            ArkHelper::assertItem(DBReq()->getCurrentSession()->isAsAdmin(), 'Not Permitted');

            $id = Ark()->webInput()->readRequest("db_id", 0);
            $addr = Ark()->webInput()->readRequest('address');
            $port = Ark()->webInput()->readRequest('port', 3306);
            $user = Ark()->webInput()->readRequest('username');
            $pass = Ark()->webInput()->readRequest('password');
            $nick = Ark()->webInput()->readRequest('db_nick');
            $is_readonly = Ark()->webInput()->readRequest('is_readonly', HostModel::ACCESS_BOTH);
            $is_available = Ark()->webInput()->readRequest('is_available', HostModel::AVAILABLE_YES);

            $id = intval($id, 10);
            $pass = base64_encode($pass);
            $fields = ['addr', 'port', 'user', 'pass', 'nick', 'is_readonly', 'is_available'];
            if (!$id) {
                // insert
                ArkHelper::assertItem($addr);
                ArkHelper::assertItem($port);
                ArkHelper::assertItem($user);
                ArkHelper::assertItem($pass);
                ArkHelper::assertItem($nick);
                ArkHelper::assertItem($is_readonly);
                ArkHelper::assertItem($is_available);
                $db_id = (new HostModel())->insert(compact($fields));
                ArkHelper::assertItem($db_id, 'Cannot create new DB');
                $this->_sayOK(['id' => $db_id]);
            } else {
                // update
                $data = [];
                foreach ($fields as $field) if (!empty($$field)) $data[] = $field;
                $afx = (new HostModel())->update(['id' => $id], compact($data));
                ArkHelper::assertItem($afx, 'Cannot update DB');
                $this->_sayOK(['afx' => $afx]);
            }
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    const DATABASE_LIST_FOR_REQUEST = "request";
    const DATABASE_LIST_FOR_MANAGEMENT = "management";

    /**
     * 获取可用的数据库列表
     * @param string $type self::DATABASE_LIST_*
     */
    public function apiDatabaseList($type)
    {
        try {
            switch ($type) {
                case self::DATABASE_LIST_FOR_MANAGEMENT:
                    ArkHelper::assertItem(DBReq()->getCurrentSession()->isAsAdmin(), 'Not Permitted');
                    $availableOnly = false;
                    break;
                default:
                    $availableOnly = true;
            }
            $list = (new HostModel())->getHostList($availableOnly);
            $this->_sayOK(['list' => $list]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }
}