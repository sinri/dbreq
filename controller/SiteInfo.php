<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/12
 * Time: 15:38
 */

namespace sinri\dbreq\controller;


use sinri\ark\web\implement\ArkWebController;

class SiteInfo extends ArkWebController
{
    public function apiHomePageInfo()
    {
        $data = [
            'title' => 'Welcome to DBReq',
            'news' => [
                'Configure your databases and users,',
                'Apply database operation requirements,',
                'Make judgement and confirm result.',
            ]
        ];
        $this->_sayOK($data);
    }
}