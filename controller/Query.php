<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 15:47
 */

namespace sinri\dbreq\controller;


use sinri\ark\web\implement\ArkWebController;
use sinri\dbreq\core\DatabaseAdapter;
use sinri\dbreq\core\DBReqCore;
use sinri\dbreq\model\HostModel;

class Query extends ArkWebController
{
    /**
     * 用户可用的速查库
     */
    public function apiAvailableDatabases()
    {
        try {
            $dbIds = DBReq()->getCurrentSession()->getUser()->getPermittedDatabasesForAuthType(DBReqCore::TYPE_QUERY);
            $dbs = [];
            if (!empty($dbIds)) {
                $dbs = (new HostModel())->selectRowsWithSort(['id' => $dbIds, 'is_available' => HostModel::AVAILABLE_YES], 'nick');
                if (empty($dbs)) $dbs = [];
                foreach ($dbs as $key => $db) {
                    unset($dbs[$key]['pass']);
                }
            }
            $this->_sayOK(['list' => $dbs]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    /**
     * 跑SQL返回查询结果
     */
    public function apiRunQuery()
    {
        $debug = [];
        try {
            $sql = Ark()->webInput()->readRequest("sql");
            $databaseId = Ark()->webInput()->readRequest("db_id");

            // check auth
            if (!DBReq()->getCurrentSession()->isAsAdmin()) {
                if (!in_array($databaseId, DBReq()->getCurrentSession()->getUser()->getPermittedDatabasesForAuthType(DBReqCore::TYPE_QUERY))) {
                    throw new \Exception("Not Permitted to query this database");
                }
            }

            $result = (new DatabaseAdapter($databaseId))->runQuickQuery($sql, $debug);

            $table = [];
            $fields = [];
            for ($i = 0; $i < count($result); $i++) {
                if ($i === 0) {
                    foreach ($result[$i] as $key => $value) {
                        $fields[] = $key;
                    }
                }
                $row = [];
                foreach ($fields as $field) {
                    $row[] = $result[$i][$field];
                }
                $table[] = $row;
            }

            $this->_sayOK(['result' => $result, 'content' => $table, 'fields' => $fields, 'debug' => $debug]);
        } catch (\Exception $exception) {
            $this->_sayFail(['error' => $exception->getMessage(), 'debug' => $debug]);
        }
    }
}