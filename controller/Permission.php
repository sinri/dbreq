<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/14
 * Time: 11:00
 */

namespace sinri\dbreq\controller;


use sinri\ark\core\ArkHelper;
use sinri\ark\web\implement\ArkWebController;
use sinri\dbreq\core\DBReqCore;
use sinri\dbreq\entity\UserEntity;
use sinri\dbreq\model\AuthMappingModel;
use sinri\dbreq\model\HostModel;
use sinri\dbreq\model\UserModel;

class Permission extends ArkWebController
{
    const UPDATE_USER_PRIVILEGE_ENABLE = "ENABLE";
    const UPDATE_USER_PRIVILEGE_DISABLE = "DISABLE";

    // old

    public function apiPermissionMatrix()
    {
        try {
            $allUsers = (new UserModel())->getUserInfoList();
            $allDatabases = (new HostModel())->getHostList(false);

            $matrix = [];
            foreach ($allUsers as $user) {
                $userEntity = new UserEntity($user['user_id']);
                $userMeta = [];
                foreach ([
                             DBReqCore::TYPE_QUERY,
                             DBReqCore::TYPE_UPDATE,
                             DBReqCore::TYPE_SELECT,
                             DBReqCore::TYPE_STRUCTURE
                         ] as $authType) {
                    $userMeta[$authType] = $userEntity->getPermittedDatabasesForAuthType($authType);
                }

                $userRow = [
                    'user' => $user,
                    'details' => [],
                ];
                foreach ($allDatabases as $database) {
                    $permission = [
                        'admin' => !!$user['is_admin'],
                        //other types
                        'query' => in_array($database['id'], $userMeta[DBReqCore::TYPE_QUERY]),
                        'select' => in_array($database['id'], $userMeta[DBReqCore::TYPE_SELECT]),
                        'update' => in_array($database['id'], $userMeta[DBReqCore::TYPE_UPDATE]),
                        'structure' => in_array($database['id'], $userMeta[DBReqCore::TYPE_STRUCTURE]),
                    ];
                    $databaseCell = [
                        'database' => $database,
                        'permission' => $permission,
                    ];
                    $userRow['details'][] = $databaseCell;
                    //ArkHelper::writeIntoArray($matrix,[$user['user_id'],$database['id']],$permission);
                }

                $matrix[] = $userRow;
            }

            $this->_sayOK([
                'users' => $allUsers,
                'databases' => $allDatabases,
                'matrix' => $matrix,
            ]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    public function apiUpdatePermissionMatrix()
    {
        try {
            ArkHelper::assertItem(DBReq()->getCurrentSession()->isAsAdmin(), 'Not Permitted');

            $applications = Ark()->webInput()->readRequest('applications', []);
            ArkHelper::assertItem(is_array($applications), 'Format Error');

            DBReq()->db()->beginTransaction();

            $model = new AuthMappingModel();

            $model->delete([]);

            foreach ($applications as $application) {
                $user_id = ArkHelper::readTarget($application, 'user_id');
                $host_id = ArkHelper::readTarget($application, 'host_id');
                $auth_type = ArkHelper::readTarget($application, 'auth_type');
                //$action = ArkHelper::readTarget($application, 'action');
                ArkHelper::assertItem($user_id);
                ArkHelper::assertItem($host_id);
                ArkHelper::assertItem(
                    in_array(
                        $auth_type,
                        [DBReqCore::TYPE_QUERY, DBReqCore::TYPE_UPDATE, DBReqCore::TYPE_SELECT, DBReqCore::TYPE_STRUCTURE]
                    )
                );
                //ArkHelper::assertItem($action);

                $data = ['user_id' => $user_id, 'host_id' => $host_id, 'auth_type' => $auth_type];

                $model->insert($data);
            }

            DBReq()->db()->commit();
            $this->_sayOK();
        } catch (\Exception $e) {
            try {
                DBReq()->db()->rollBack();
            } catch (\Exception $e) {
                // who cares
            }
            $this->_sayFail($e->getMessage());
        }
    }

    /**
     * 更新用户在各数据库的权限。发起申请的权限为基本人权不需授权就有。
     * @deprecated
     */
    public function apiUpdateUserPrivilege()
    {
        try {
            ArkHelper::assertItem(DBReq()->getCurrentSession()->isAsAdmin(), 'Not Permitted');

            $applications = Ark()->webInput()->readRequest('applications', []);
            ArkHelper::assertItem(is_array($applications), 'Format Error');

            DBReq()->db()->beginTransaction();

            $model = new AuthMappingModel();

            foreach ($applications as $application) {
                $user_id = ArkHelper::readTarget($application, 'user_id');
                $host_id = ArkHelper::readTarget($application, 'host_id');
                $auth_type = ArkHelper::readTarget($application, 'auth_type');
                $action = ArkHelper::readTarget($application, 'action');
                ArkHelper::assertItem($user_id);
                ArkHelper::assertItem($host_id);
                ArkHelper::assertItem($auth_type);
                ArkHelper::assertItem($action);

                $data = ['user_id' => $user_id, 'host_id' => $host_id, 'auth_type' => $auth_type];

                if ($action === self::UPDATE_USER_PRIVILEGE_DISABLE) {
                    $model->delete($data);
                } elseif ($action === self::UPDATE_USER_PRIVILEGE_ENABLE) {
                    $model->replace($data);
                } else {
                    continue;
                }
            }

            DBReq()->db()->commit();
            $this->_sayOK();
        } catch (\Exception $e) {
            try {
                DBReq()->db()->rollBack();
            } catch (\Exception $e) {
                // who cares
            }
            $this->_sayFail($e->getMessage());
        }
    }

}