<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 15:44
 */

namespace sinri\dbreq\controller;


use sinri\ark\core\ArkHelper;
use sinri\ark\database\model\ArkSQLCondition;
use sinri\ark\web\implement\ArkWebController;
use sinri\dbreq\core\DatabaseAdapter;
use sinri\dbreq\core\DBReqCore;
use sinri\dbreq\entity\IssueEntity;
use sinri\dbreq\model\IssueModel;

class Request extends ArkWebController
{
    /**
     * @param int[] $issueIds
     * @return array
     * @throws \Exception
     */
    protected function fetchIssuesToList($issueIds)
    {
        $list = [];
        foreach ($issueIds as $issueId) {
            $issue = new IssueEntity($issueId);
            $list[$issueId] = $issue->getDetails();
        }
        return array_values($list);
    }

    /**
     * 获取请求列表
     */
    public function apiRequestList()
    {
        try {
            $conditions = [];

            $fields = [
                'title',
                'status',
                'update_time',
                'author_id',
                'host_id',
                'type',
            ];
            foreach ($fields as $field) {
                $value = Ark()->webInput()->readRequest($field, null, '/^.+$/');
                if ($value !== null) {
                    $conditions[$field] = $value;
                }
            }

            $pageNumber = Ark()->webInput()->readRequest("page_number", 1);
            $pageSize = Ark()->webInput()->readRequest("page_size", 10);
            $issueIds = (new IssueModel())->getIssueIdListWithConditions($conditions, $pageNumber, $pageSize);
            $list = $this->fetchIssuesToList($issueIds);
            $total = (new IssueModel())->getIssueCountForConditions($conditions);

            $this->_sayOK(['list' => array_values($list), 'total' => $total]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    public function apiRequestListForApprove()
    {
        try {
            $pageNumber = Ark()->webInput()->readRequest("page_number", 1);
            $pageSize = Ark()->webInput()->readRequest("page_size", 10);
            if (DBReq()->getCurrentSession()->isAsAdmin()) {
                //all in
                $issueIds = (new IssueModel())->getIssueIdListWithConditions(
                    ['status' => IssueModel::STATUS_PENDING], $pageNumber, $pageSize
                );
                $list = $this->fetchIssuesToList($issueIds);
                $total = (new IssueModel())->getIssueCountForConditions(['status' => IssueModel::STATUS_PENDING]);
            } else {
                $conditions = [];
                $conditions[] = (new ArkSQLCondition('status', ArkSQLCondition::OP_EQ, IssueModel::STATUS_PENDING))->makeConditionSQL();

                foreach ([
                             DBReqCore::TYPE_UPDATE,
                             DBReqCore::TYPE_SELECT,
                             DBReqCore::TYPE_STRUCTURE,
                         ] as $authType) {
                    $hostIds = DBReq()->getCurrentSession()->getUser()->getPermittedDatabasesForAuthType($authType);
                    if (empty($hostIds)) continue;
                    $typeCondition = (new ArkSQLCondition('type', ArkSQLCondition::OP_EQ, $authType))->makeConditionSQL();
                    $hostCondition = (new ArkSQLCondition('host_id', ArkSQLCondition::OP_IN, $hostIds))->makeConditionSQL();
                    $conditions[] = "(" . $typeCondition . " and " . $hostCondition . ")";
                }
                $issueIds = (new IssueModel())->selectRowsWithSort(
                    implode(" and ", $conditions),
                    'id desc',
                    $pageSize,
                    ($pageNumber - 1) * $pageSize
                );
                $list = $this->fetchIssuesToList($issueIds);

                $total = (new IssueModel())->selectRowsForCount($conditions);
            }
            $this->_sayOK(['list' => array_values($list), 'total' => $total]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    /**
     * 创建新的请求
     */
    public function apiCreateRequest()
    {
        try {
            $data = [
                'create_time' => time(),
                'update_time' => time(),
                'author_id' => DBReq()->getCurrentSession()->getUser()->getUserId(),
                'status' => IssueModel::STATUS_PENDING,
            ];

            $fields = [
                'title',
                'host_id',
                'type',
                'description',
                'query',
            ];
            foreach ($fields as $field) {
                $value = Ark()->webInput()->readRequest($field, null, '/^.+$/');
                if ($value !== null) {
                    $data[$field] = $value;
                } else {
                    throw new \Exception("Missing field: " . $field);
                }
            }

            // validate SQL
            if (!(new DatabaseAdapter($data['host_id']))->validateSql($data['query'], $data['type'], $error_list)) {
                throw new \Exception("SYNTAX ERROR: " . PHP_EOL . implode(PHP_EOL, $error_list), 1);
            }

            $newIssueId = (new IssueModel())->insert($data);
            ArkHelper::assertItem($newIssueId, 'cannot create new issue');
            $this->_sayOK(['issue_id' => $newIssueId]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    /**
     * 获取详情
     * @param $issueId
     */
    public function apiRequestDetail($issueId)
    {
        try {
            $issue = new IssueEntity($issueId);
            $detail = $issue->getDetails(true);
            $detail['can_deal'] = $issue->canUserDeal(DBReq()->getCurrentSession()->getUser());
            $detail['can_edit'] = $issue->canUserEdit(DBReq()->getCurrentSession()->getUser());
            $detail['can_cancel'] = $issue->canUserCancel(DBReq()->getCurrentSession()->getUser());
            $this->_sayOK(['issue' => $detail]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    public function apiDownloadRequestAttachment()
    {
        try {
            $issueId = Ark()->webInput()->readRequest("issue_id");
            $issue = new IssueEntity($issueId);
            ArkHelper::assertItem($issue, 'no such issue');
            $path = $issue->getAttachmentFilePath();
            if (!file_exists($path)) {
                throw new \Exception("Not Found", 404);
            }
            Ark()->webOutput()->downloadFileIndirectly($path);
        } catch (\Exception $exception) {
            Ark()->webOutput()->responseHTTPCode($exception->getCode() ? $exception->getCode() : 500);
            echo $exception->getMessage();
        }
    }

    public function apiPreviewRequestAttachment()
    {
        try {
            $issueId = Ark()->webInput()->readRequest("issue_id");
            $issue = new IssueEntity($issueId);
            ArkHelper::assertItem($issue, 'no such issue');
            $path = $issue->getAttachmentFilePath();
            if (!file_exists($path)) {
                throw new \Exception("Not Found", 404);
            }
            $handle = @fopen($path, "r");
            $csv = [];
            if ($handle) {
                $max_lines = 10;
                while (($buffer = fgets($handle, 4096)) !== false && $max_lines >= 0) {
                    $csv[] = str_getcsv($buffer);
                    $max_lines--;
                }
                //if (!feof($handle)) {
                //    echo "Error: unexpected fgets() fail\n";
                //}
                fclose($handle);
            } else {
                throw new \Exception("cannot get reader");
            }
            $this->_sayOK(['csv' => $csv]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    /**
     * 编辑
     */
    public function apiRequestUpdate()
    {
        try {
            $issueId = Ark()->webInput()->readRequest("issue_id");
            $issue = new IssueEntity($issueId);
            ArkHelper::assertItem($issue, 'no such issue');
            ArkHelper::assertItem(
                $issue->canUserEdit(DBReq()->getCurrentSession()->getUser()),
                'issue in this status not editable'
            );

            $data = [
                'update_time' => time(),
                'status' => IssueModel::STATUS_PENDING,
            ];

            $fields = [
                'title',
                'host_id',
                'type',
                'description',
                'query',
            ];
            foreach ($fields as $field) {
                $value = Ark()->webInput()->readRequest($field, null, '/^.+$/');
                if ($value !== null) {
                    $data[$field] = $value;
                }
            }

            // validate SQL
            if (!(new DatabaseAdapter($data['host_id']))->validateSql($data['query'], $data['type'], $error_list)) {
                throw new \Exception("SYNTAX ERROR: " . PHP_EOL . implode(PHP_EOL, $error_list), 1);
            }

            $afx = (new IssueModel())->update(['id' => $issue->getIssueMeta()->id], $data);
            ArkHelper::assertItem($afx, 'cannot update issue');
            $this->_sayOK(['$afx' => $afx]);
        } catch (\Exception $exception) {
            $this->_sayFail($exception->getMessage());
        }
    }

    /**
     * 同意请求
     */
    public function apiRequestApprove()
    {
        try {
            $issueId = Ark()->webInput()->readRequest("issue_id");
            $issue = new IssueEntity($issueId);
            ArkHelper::assertItem(
                $issue->canUserDeal(DBReq()->getCurrentSession()->getUser()),
                'Cannot Approve'
            );
            DBReq()->db()->beginTransaction();
            $afx = $issue->getIssueMeta()->update(
                ['id' => $issue->getIssueMeta()->id, 'status' => IssueModel::STATUS_PENDING],
                [
                    'status' => IssueModel::STATUS_QUEUED,
                    'checker_id' => DBReq()->getCurrentSession()->getUser()->getUserId(),
                    'update_time' => time(),
                ]
            );
            ArkHelper::assertItem($afx);
            DBReq()->db()->commit();
            $this->_sayOK(['afx' => $afx]);
        } catch (\Exception $exception) {
            try {
                DBReq()->db()->rollBack();
            } catch (\Exception $e) {
                //who cares
            }
            $this->_sayFail($exception->getMessage());
        }
    }

    /**
     * 拒绝请求
     */
    public function apiRequestDeny()
    {
        try {
            $issueId = Ark()->webInput()->readRequest("issue_id");
            $issue = new IssueEntity($issueId);
            ArkHelper::assertItem(
                $issue->canUserDeal(DBReq()->getCurrentSession()->getUser()),
                'Cannot Deny'
            );
            DBReq()->db()->beginTransaction();
            $afx = $issue->getIssueMeta()->update(
                ['id' => $issue->getIssueMeta()->id, 'status' => IssueModel::STATUS_PENDING],
                [
                    'status' => IssueModel::STATUS_DENIED,
                    'checker_id' => DBReq()->getCurrentSession()->getUser()->getUserId(),
                    'update_time' => time(),
                ]
            );
            ArkHelper::assertItem($afx);
            DBReq()->db()->commit();
            $this->_sayOK(['afx' => $afx]);
        } catch (\Exception $exception) {
            try {
                DBReq()->db()->rollBack();
            } catch (\Exception $e) {
                //who cares
            }
            $this->_sayFail($exception->getMessage());
        }
    }

    /**
     * 取消请求
     */
    public function apiRequestCancel()
    {
        try {
            $issueId = Ark()->webInput()->readRequest("issue_id");
            $issue = new IssueEntity($issueId);
            ArkHelper::assertItem(
                $issue->canUserCancel(DBReq()->getCurrentSession()->getUser()),
                'Cannot Cancel'
            );
            DBReq()->db()->beginTransaction();
            $afx = $issue->getIssueMeta()->update(
                ['id' => $issue->getIssueMeta()->id, 'status' => IssueModel::STATUS_PENDING],
                [
                    'status' => IssueModel::STATUS_DENIED,
                    'checker_id' => DBReq()->getCurrentSession()->getUser()->getUserId(),
                    'update_time' => time(),
                ]
            );
            ArkHelper::assertItem($afx);
            DBReq()->db()->commit();
            $this->_sayOK(['afx' => $afx]);
        } catch (\Exception $exception) {
            try {
                DBReq()->db()->rollBack();
            } catch (\Exception $e) {
                //who cares
            }
            $this->_sayFail($exception->getMessage());
        }
    }
}