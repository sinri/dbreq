<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 14:05
 */

namespace sinri\dbreq\controller;


use sinri\ark\core\ArkHelper;
use sinri\ark\web\implement\ArkWebController;
use sinri\dbreq\model\UserModel;

class Security extends ArkWebController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 登录获取token。通过配置的插件来进行。
     */
    public function apiRequestToken()
    {
        try {
            $plugin = DBReq()->getUserPlugin();
            //echo $plugin::pluginCategory();
            $session = $plugin->createSession();
            $this->_sayOK(['token' => $session->getToken(), 'expire' => $session->getExpire()]);
        } catch (\Exception $e) {
            $this->_sayFail($e->getMessage());
        }
    }

    /**
     * 登出时灭杀token
     */
    public function apiDisableToken()
    {
        try {
            $done = DBReq()->getCurrentSession()->closeSession();
            ArkHelper::assertItem($done);
            $this->_sayOK();
        } catch (\Exception $e) {
            $this->_sayFail($e->getMessage());
        }
    }

    public function apiUserList()
    {
        try {
            $list = (new UserModel())->getUserInfoList();
            $this->_sayOK(['list' => $list]);
        } catch (\Exception $e) {
            $this->_sayFail($e->getMessage());
        }
    }

    /**
     * 更新(insert/update)用户信息
     */
    public function apiUpdateUser()
    {
        try {
            ArkHelper::assertItem(DBReq()->getCurrentSession()->isAsAdmin(), 'Not Permitted');
            DBReq()->getUserPlugin()->updateUser();
            $this->_sayOK();
        } catch (\Exception $e) {
            $this->_sayFail($e->getMessage());
        }
    }


}