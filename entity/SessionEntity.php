<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 23:33
 */

namespace sinri\dbreq\entity;


use sinri\dbreq\model\SessionModel;

class SessionEntity
{
    /**
     * @var string
     */
    protected $token;
    /**
     * @var int
     */
    protected $expire;
    /**
     * @var UserEntity
     */
    protected $user;

    public function __construct($token = null)
    {
        $this->token = $token ? $token : '';
        $this->expire = 0;
        $this->user = null;
    }

    /**
     * @param UserEntity $user
     * @return SessionEntity|false
     */
    public static function createSessionForUser($user)
    {
        $token = uniqid("DBREQ-", true);
        $expire = (time() + DBReq()->readConfig(['session_life'], 3600));
        $new_id = (new SessionModel())->insert([
            'user_id' => $user->getUserId(),
            'token' => $token,
            "expire" => $expire,
        ]);
        if (!$new_id) return false;

        $session = new SessionEntity($token);
        $session->expire = $expire;
        $session->user = $user;
        return $session;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @return int
     */
    public function getExpire(): int
    {
        return $this->expire;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @return bool
     */
    public function isAsAdmin(): bool
    {
        if (is_a($this->user, UserEntity::class)) {
            return $this->user->isAdmin();
        }
        return false;
    }

    /**
     * @throws \Exception
     */
    public function loadSession()
    {
        $sessionModel = SessionModel::loadWithToken($this->token);
        if (!$sessionModel) {
            throw new \Exception("You are not permitted to access to this resource with token " . json_encode($this->token));
        }
        $this->expire = $sessionModel->expire;
        $this->user = new UserEntity($sessionModel->user_id);
        if ($this->user === null) {
            throw new \Exception("You are not permitted to access to this resource.");
        }
    }

    /**
     * @return bool
     */
    public function closeSession()
    {
        $this->expire = time();
        $afx = (new SessionModel())->update(['token' => $this->getToken()], ['expire' => $this->expire]);
        return !!$afx;
    }
}