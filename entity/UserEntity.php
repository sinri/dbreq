<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 15:50
 */

namespace sinri\dbreq\entity;


use sinri\ark\core\ArkHelper;
use sinri\dbreq\model\AuthMappingModel;
use sinri\dbreq\model\HostModel;
use sinri\dbreq\model\UserModel;

class UserEntity
{
    protected $userId;
    protected $userName;
    protected $realName;
    protected $isAdmin;
    protected $passwordHash;

    /**
     * UserEntity constructor.
     * @param int $user_id
     * @throws \Exception
     */
    public function __construct($user_id)
    {
        // make instance
        $this->userId = 0;
        $this->userName = 'Anonymous';
        $this->realName = 'Anonymous';
        $this->isAdmin = false;
        $this->passwordHash = '';

        if ($user_id > 0) {
            $this->initialize($user_id);
        }
    }

    /**
     * @param $user_id
     * @throws \Exception
     */
    protected function initialize($user_id)
    {
        $user_model = new UserModel();
        $row = $user_model->selectRow(['user_id' => $user_id]);
        ArkHelper::assertItem($row);
        $user_model->loadFieldsFromRowArray($row);
        $this->userId = $user_model->user_id;
        $this->userName = $user_model->user_name;
        $this->realName = $user_model->real_name;
        $this->isAdmin = !!$user_model->is_admin;
        $this->passwordHash = $user_model->password;
    }

    /**
     * @param string $username
     * @return null|UserEntity
     */
    public static function buildInstanceWithUsername($username)
    {
        try {
            $model = new UserModel();
            $row = $model->selectRow(['user_name' => $username]);
            ArkHelper::assertItem($row);
            $model->loadFieldsFromRowArray($row);
            $user = new UserEntity($model->user_id);
            return $user;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getRealName(): string
    {
        return $this->realName;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @param string $authType use AuthMappingModel::TYPE_*
     * @return int[]
     */
    public function getPermittedDatabasesForAuthType($authType)
    {
        $entireList = (new HostModel())->selectRows(['is_available' => HostModel::AVAILABLE_YES]);
        if (empty($entireList)) return [];
        $dbIdList = array_column($entireList, 'id');
        if (!$this->isAdmin()) {
            $permittedList = (new AuthMappingModel())->selectRows(['user_id' => $this->userId, 'auth_type' => $authType]);
            if (empty($permittedList)) return [];
            $permittedIdList = array_column($permittedList, 'host_id');
            $dbIdList = array_intersect($dbIdList, $permittedIdList);
        }
        return $dbIdList;
    }
}