<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/9
 * Time: 21:46
 */

namespace sinri\dbreq\entity;


use sinri\ark\core\ArkHelper;
use sinri\dbreq\core\DatabaseAdapter;
use sinri\dbreq\core\DBReqCore;
use sinri\dbreq\core\SQLChecker;
use sinri\dbreq\model\HostModel;
use sinri\dbreq\model\IssueModel;

class IssueEntity
{
    protected $issueMeta;
    protected $databaseMeta;
    protected $applyUser;
    protected $dealUser;

    /**
     * IssueEntity constructor.
     * @param int $issueId
     * @throws \Exception
     */
    public function __construct($issueId)
    {
        $this->issueMeta = new IssueModel();
        $row = $this->issueMeta->selectRow(['id' => $issueId]);
        ArkHelper::assertItem($row, 'Cannot build issue entity.');
        $this->issueMeta->loadFieldsFromRowArray($row);
    }

    /**
     * @return IssueModel
     */
    public function getIssueMeta(): IssueModel
    {
        return $this->issueMeta;
    }

    /**
     * @return string
     */
    public function readableIssueType()
    {
        switch ($this->issueMeta->type) {
            case DBReqCore::TYPE_UPDATE:
                return "MODIFICATION";
            case DBReqCore::TYPE_SELECT:
                return "READ";
            case DBReqCore::TYPE_STRUCTURE:
                return "STRUCTURE";
            default:
                return "UNKNOWN";
        }
    }

    /**
     * @return string
     */
    public function readableStatusType()
    {
        switch ($this->issueMeta->status) {
            case IssueModel::STATUS_PENDING:// 待审核: 0
                return "PENDING";
            case IssueModel::STATUS_DENIED:// 已拒绝: 1
                return "DENIED";
            case IssueModel::STATUS_QUEUED:// 待执行: 2
                return "QUEUED";
            case IssueModel::STATUS_DONE:// 执行成功: 3
                return "DONE";
            case IssueModel::STATUS_ERROR:// 执行出错: 4
                return "ERROR";
            case IssueModel::STATUS_CANCEL:// 已取消: 5
                return "CANCEL";
            case IssueModel::STATUS_RUNNING:// 执行中: 6
                return "RUNNING";
            default:
                return "UNKNOWN";
        }
    }

    /**
     * @return UserEntity
     * @throws \Exception
     */
    public function getApplyUser()
    {
        if (!$this->applyUser) $this->applyUser = new UserEntity($this->issueMeta->author_id);
        return $this->applyUser;
    }

    /**
     * @return UserEntity
     * @throws \Exception
     */
    public function getDealUser()
    {
        if (!$this->dealUser) {
            if ($this->issueMeta->checker_id) {
                $this->dealUser = new UserEntity($this->issueMeta->checker_id);
            } else {
                $this->dealUser = new UserEntity(0);
            }
        }
        return $this->dealUser;
    }

    /**
     * @return HostModel
     * @throws \Exception
     */
    public function getDatabaseMeta()
    {
        if (!$this->databaseMeta) {
            $this->databaseMeta = new HostModel();
            $row = $this->databaseMeta->selectRow(['id' => $this->issueMeta->host_id]);
            ArkHelper::assertItem($row);
            $this->databaseMeta->loadFieldsFromRowArray($row);
        }
        return $this->databaseMeta;
    }

    /**
     * @param UserEntity $user
     * @return bool
     */
    public function canUserDeal($user)
    {
        if ($this->getIssueMeta()->status !== IssueModel::STATUS_PENDING) return false;
        if ($user->isAdmin()) return true;
        $list = $user->getPermittedDatabasesForAuthType($this->getIssueMeta()->type);
        return in_array($this->getIssueMeta()->host_id, $list);
    }

    /**
     * @param UserEntity $user
     * @return bool
     */
    public function canUserEdit($user)
    {
        if (!in_array($this->getIssueMeta()->status, [IssueModel::STATUS_DENIED, IssueModel::STATUS_ERROR])) return false;
        if ($user->getUserId() !== $this->getIssueMeta()->author_id) return false;
        return true;
    }

    /**
     * @param UserEntity $user
     * @return bool
     */
    public function canUserCancel($user)
    {
        if (!in_array($this->getIssueMeta()->status, [IssueModel::STATUS_PENDING])) return false;
        if ($user->getUserId() !== $this->getIssueMeta()->author_id) return false;
        return true;
    }

    /**
     * @return string
     */
    protected function getMessageFilePath()
    {
        $storageDir = DBReq()->readConfig(['storage_dir'], __DIR__ . '/../runtime/storage');
        return $storageDir . '/' . $this->getIssueMeta()->id . ".log";
    }

    /**
     * @return bool|string
     */
    public function getFeedbackMessage()
    {
        $path = $this->getMessageFilePath();
        if (!file_exists($path)) return '';
        return @file_get_contents($path);
    }

    /**
     * @param string $message
     * @return bool|int
     */
    public function writeFeedbackMessage($message)
    {
        $file = $this->getMessageFilePath();
        return file_put_contents($file, $message);
    }

    /**
     * @return string
     */
    public function getAttachmentFilePath()
    {
        $storageDir = DBReq()->readConfig(['storage_dir'], __DIR__ . '/../runtime/storage');
        return $storageDir . '/' . $this->getIssueMeta()->id . ".csv";
    }

    /**
     * @param $exportedTemplateFile
     * @return bool
     */
    public function writeFeedbackAttachment($exportedTemplateFile)
    {
        return rename($exportedTemplateFile, $this->getAttachmentFilePath());
    }

    /**
     * @param bool $needAdvancedInfo
     * @return array
     * @throws \Exception
     */
    public function getDetails($needAdvancedInfo = false)
    {
        // basic
        $data = [
            "issue_id" => $this->getIssueMeta()->id,
            'title' => $this->getIssueMeta()->title,
            'type' => $this->getIssueMeta()->type,
            'type_desc' => $this->readableIssueType(),
            'status' => $this->getIssueMeta()->status,
            'status_desc' => $this->readableStatusType(),
            'database' => [
                "host_id" => $this->getDatabaseMeta()->id,
                "host_nick" => $this->getDatabaseMeta()->nick,
                "is_readonly" => $this->getDatabaseMeta()->is_readonly,
            ],
            "author_id" => $this->getIssueMeta()->author_id,
            "author_username" => $this->getApplyUser()->getUserName(),
            "author_realname" => $this->getApplyUser()->getRealName(),
            "update_time" => date('Y-m-d H:i:s', $this->getIssueMeta()->update_time),
        ];
        // advance
        if ($needAdvancedInfo) {
            $data['query'] = $this->getIssueMeta()->query;
            $data['description'] = $this->getIssueMeta()->description;
            $data['create_time'] = date('Y-m-d H:i:s', intval($this->getIssueMeta()->create_time, 10));
            $data['execute_time'] = (
            intval($this->getIssueMeta()->execute_time, 10) > 0 ?
                date('Y-m-d H:i:s', intval($this->getIssueMeta()->execute_time, 10)) :
                0
            );
            $data['checker_id'] = $this->getDealUser()->getUserId();
            $data['checker_username'] = $this->getDealUser()->getUserName();
            $data['checker_realname'] = $this->getDealUser()->getRealName();

            $data['feedback'] = $this->getFeedbackMessage();
            $data['has_attachment'] = file_exists($this->getAttachmentFilePath());
        }
        return $data;
    }

    /**
     *
     */
    public function execute()
    {
        $infoLines = [];
        try {
            $this->writeFeedbackMessage("");

            $adapter = new DatabaseAdapter($this->getDatabaseMeta()->id);

            $checker = new SQLChecker();
            // SPECIAL HANDLER FOR CALL STATEMENT
            $type = $this->issueMeta->type;
            if (
                $checker->checkIfAvailableCallStatement($this->issueMeta->query)
                && $type == DBReqCore::TYPE_STRUCTURE
            ) {
                $type = DBReqCore::SPECIAL_TYPE_CALL_STATEMENT;
            }

            $sqlBeginTime = microtime(true);

            $context = [
                "issue_id" => $this->issueMeta->id,
            ];
            DBReq()->loggerForCli()->info("SQL: " . $this->issueMeta->query, $context);

            switch ($type) {
                case DBReqCore::TYPE_SELECT:
                    DBReq()->loggerForCli()->info("Begin SQL Export", $context);
                    $export = $adapter->queryAndExportWithMySQLi($this->issueMeta->query, $errors);
                    if ($export === false) {
                        throw new \Exception("Error Reported:" . PHP_EOL . implode(PHP_EOL, $errors));
                    }
                    $this->writeFeedbackAttachment($export);
                    break;
                case DBReqCore::SPECIAL_TYPE_CALL_STATEMENT:
                    DBReq()->loggerForCli()->info("Begin SQL Call", $context);
                    $done = $adapter->executeCall($this->issueMeta->query, $errors);
                    if (!$done) {
                        throw new \Exception("Error Reported:" . PHP_EOL . implode(PHP_EOL, $errors));
                    }
                    break;
                default:
                    DBReq()->loggerForCli()->info("Begin SQL Query", $context);
                    $done = $adapter->executeMulti($this->issueMeta->query, $this->issueMeta->type, $affected, $errors);
                    if (!$done) {
                        throw new \Exception("Error Reported:" . PHP_EOL . implode(PHP_EOL, $errors));
                    }
                    $totalAffect = 0;
                    $sqlIdx = 1;
                    foreach ($affected as $singleAffect) {
                        $totalAffect += $singleAffect;
                        $infoLines[] = "第" . $sqlIdx . "句：影响" . $singleAffect . "行数据";
                        $sqlIdx++;
                    }
                    $infoLines[] = "共影响" . $totalAffect . "行数据";
                    break;
            }

            $sqlEndTime = microtime(true);

            array_unshift($infoLines, "Time Cost: " . number_format($sqlEndTime - $sqlBeginTime, 4) . " seconds");

            if (!empty($errors)) {
                DBReq()->loggerForCli()->error('Writable error got: ' . json_encode($errors, JSON_UNESCAPED_UNICODE));
                $infoLines[] = "Errors:";
                foreach ($errors as $line => $text) {
                    $infoLines[] = "第" . $line . "句：" . $text;
                }
            }

            $this->writeFeedbackMessage(implode(PHP_EOL, $infoLines));

            //更新issue
            $this->recordIssueExecuted(false);

            DBReq()->loggerForCli()->info('似乎大事已成 #' . $this->issueMeta->id);
        } catch (\Exception $exception) {
            $this->writeFeedbackMessage($exception->getMessage());
            $this->recordIssueExecuted(true);
            DBReq()->loggerForCli()->error('似乎大势已去 #' . $this->issueMeta->id);
        }
    }

    /**
     * @param bool $failed
     * @return int
     */
    protected function recordIssueExecuted($failed = false)
    {
        $afx = $this->issueMeta->update(
            ['id' => $this->issueMeta->id],
            ['execute_time' => time(), 'status' => ($failed ? IssueModel::STATUS_ERROR : IssueModel::STATUS_DONE)]
        );
        return $afx;
    }
}