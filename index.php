<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 15:52
 */

require_once __DIR__ . '/autoload.php';
date_default_timezone_set("Asia/Shanghai");
$web_service = Ark()->webService();
$web_service->getRouter()->setErrorHandler(
    \sinri\ark\web\ArkRouteErrorHandler::buildWithCallback(
        function ($error) {
            Ark()->webOutput()->setContentTypeHeader(\sinri\ark\io\ArkWebOutput::CONTENT_TYPE_JSON);
            Ark()->webOutput()->jsonForAjax(\sinri\ark\io\ArkWebOutput::AJAX_JSON_CODE_FAIL, $error);
        }
    )
);
$web_service->getRouter()->loadAllControllersInDirectoryAsCI(
    __DIR__ . '/controller',
    'api/',
    'sinri\dbreq\controller\\',
    [
        \sinri\dbreq\core\SecurityFilter::class,
    ]
);
$web_service->getRouter()->get("", function () {
    header("Location: frontend/");
});
$web_service->handleRequest();