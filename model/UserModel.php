<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:48
 */

namespace sinri\dbreq\model;


use sinri\ark\database\model\ArkDatabaseTableModel;
use sinri\ark\database\pdo\ArkPDO;

/**
 * Class UserModel
 * @package sinri\dbreq\model
 * @property integer user_id
 * @property integer is_admin
 * @property string user_name
 * @property string real_name
 * @property string password
 */
class UserModel extends ArkDatabaseTableModel
{

    /**
     * @return ArkPDO
     * @throws \Exception
     */
    public function db()
    {
        return DBReq()->db();
    }

    /**
     * @return string
     */
    protected function mappingTableName()
    {
        return "req_user";
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return array|bool
     */
    public function getUserInfoList($limit = 0, $offset = 0)
    {
        $list = $this->selectRowsWithSort([], 'user_id asc', $limit, $offset);
        foreach ($list as $key => $item) {
            unset($list[$key]['password']);
        }
        return $list;
    }
}