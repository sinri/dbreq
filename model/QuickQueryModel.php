<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:49
 */

namespace sinri\dbreq\model;


use sinri\ark\database\model\ArkDatabaseTableModel;
use sinri\ark\database\pdo\ArkPDO;

/**
 * Class QuickQueryModel
 * @package sinri\dbreq\model
 * @property integer query_id
 * @property integer user_id
 * @property integer host_id
 * @property string query
 * @property string query_time
 * @property float duration
 */
class QuickQueryModel extends ArkDatabaseTableModel
{

    /**
     * @return ArkPDO
     * @throws \Exception
     */
    public function db()
    {
        return DBReq()->db();
    }

    /**
     * @return string
     */
    protected function mappingTableName()
    {
        return "req_quick_query";
    }

    /**
     * @param int $userId
     * @param int $hostId
     * @param string $query
     * @return bool|string
     */
    public function recordQuickQuery($userId, $hostId, $query)
    {
        return $this->insert(['user_id' => $userId, 'host_id' => $hostId, 'query' => $query]);
    }

    /**
     * @param int $query_id
     * @param float $duration
     * @return int
     */
    public function feedbackQueryQueryDuration($query_id, $duration)
    {
        return $this->update(['query_id' => $query_id], ['duration' => $duration]);
    }
}