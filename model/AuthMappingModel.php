<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:49
 */

namespace sinri\dbreq\model;


use sinri\ark\database\model\ArkDatabaseTableModel;
use sinri\ark\database\pdo\ArkPDO;

/**
 * Class AuthMappingModel
 * @package sinri\dbreq\model
 * @property integer user_id
 * @property integer host_id
 * @property integer auth_type
 */
class AuthMappingModel extends ArkDatabaseTableModel
{

//    const TYPE_QUERY = -1;//快速查询: -1
//    const TYPE_UPDATE = 0;//更新请求: 0
//    const TYPE_SELECT = 1;//导出请求: 1
//    const TYPE_STRUCTURE = 2;//结构请求: 2

    /**
     * @return ArkPDO
     * @throws \Exception
     */
    public function db()
    {
        return DBReq()->db();
    }

    /**
     * @return string
     */
    protected function mappingTableName()
    {
        return "req_auth_mapping";
    }
}