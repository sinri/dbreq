<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:49
 */

namespace sinri\dbreq\model;


use sinri\ark\database\model\ArkDatabaseTableModel;
use sinri\ark\database\pdo\ArkPDO;

/**
 * Class IssueModel
 * @package sinri\dbreq\model
 * @property integer id
 * @property string title
 * @property string description
 * @property string query
 * @property integer status
 * @property integer create_time
 * @property integer update_time
 * @property integer execute_time
 * @property integer author_id
 * @property integer host_id
 * @property integer type
 * @property integer checker_id
 */
class IssueModel extends ArkDatabaseTableModel
{

//    const TYPE_UPDATE = 0;//更新请求: 0
//    const TYPE_SELECT = 1;//导出请求: 1
//    const TYPE_STRUCTURE = 2;//结构请求: 2
//    const SPECIAL_TYPE_CALL_STATEMENT = 3;//专门搞一个类型给CALL

    const STATUS_PENDING = 0;// 待审核: 0
    const STATUS_DENIED = 1;// 已拒绝: 1
    const STATUS_QUEUED = 2;// 待执行: 2
    const STATUS_DONE = 3;// 执行成功: 3
    const STATUS_ERROR = 4;// 执行出错: 4
    const STATUS_CANCEL = 5;// 已取消: 5
    const STATUS_RUNNING = 6;// 执行中: 6

    /**
     * @return ArkPDO
     * @throws \Exception
     */
    public function db()
    {
        return DBReq()->db();
    }

    /**
     * @return string
     */
    protected function mappingTableName()
    {
        return "req_issue";
    }

    /**
     * @param array $conditions
     * @param int $pageNumber
     * @param int $pageSize
     * @return int[]
     */
    public function getIssueIdListWithConditions($conditions = [], $pageNumber = 1, $pageSize = 10)
    {
        $sql = "SELECT id FROM " . $this->getTableExpressForSQL() . " WHERE 1 ";
        $data = [];
        foreach ($conditions as $conditionKey => $conditionValue) {
            if ($conditionValue === '') continue;
            switch ($conditionKey) {
                case 'title':
                    $sql .= " and title like concat('%',?,'%') ";
                    $data[] = $conditionValue;
                    break;
                case 'status':
                    $sql .= " and status = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'update_time':
                    $sql .= " and update_time = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'author_id':
                    $sql .= " and author_id = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'host_id':
                    $sql .= " and host_id = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'type':
                    $sql .= " and type = ? ";
                    $data[] = $conditionValue;
                    break;
            }
        }
        $sql .= " order by id desc limit " . intval($pageSize, 10) . " offset " . ((intval($pageNumber, 10) - 1) * intval($pageSize, 10));
        try {
            $list = $this->db()->safeQueryAll($sql, $data);
        } catch (\Exception $e) {
            $list = [];
        }
        if (empty($list)) return [];
        else return array_column($list, 'id');
    }

    /**
     * @param array $conditions
     * @return int
     */
    public function getIssueCountForConditions($conditions = [])
    {
        $sql = "SELECT count(*) FROM " . $this->getTableExpressForSQL() . " WHERE 1 ";
        $data = [];
        foreach ($conditions as $conditionKey => $conditionValue) {
            if ($conditionValue === '') continue;
            switch ($conditionKey) {
                case 'title':
                    $sql .= " and title like concat('%',?,'%') ";
                    $data[] = $conditionValue;
                    break;
                case 'status':
                    $sql .= " and status = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'update_time':
                    $sql .= " and update_time = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'author_id':
                    $sql .= " and author_id = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'host_id':
                    $sql .= " and host_id = ? ";
                    $data[] = $conditionValue;
                    break;
                case 'type':
                    $sql .= " and type = ? ";
                    $data[] = $conditionValue;
                    break;
            }
        }
        try {
            $count = $this->db()->safeQueryOne($sql, $data);
        } catch (\Exception $e) {
            $count = -1;
        }
        return $count;
    }
}