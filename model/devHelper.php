<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:52
 */

require_once __DIR__ . '/../autoload.php';

$classes = [
    \sinri\dbreq\model\AuthMappingModel::class,
    \sinri\dbreq\model\HostModel::class,
    \sinri\dbreq\model\IssueModel::class,
    \sinri\dbreq\model\QuickQueryModel::class,
    \sinri\dbreq\model\UserModel::class,
    \sinri\dbreq\model\SessionModel::class,
];
foreach ($classes as $class) {
    $model = new $class();
    /** @noinspection PhpUndefinedMethodInspection */
    $model->devShowFieldsForPHPDoc();
}