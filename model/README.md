# Database Table Model Introduction

```sql

CREATE TABLE `req_host` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addr` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `port` int(11) DEFAULT '3306',
  `user` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `pass` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `nick` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_readonly` tinyint(1) DEFAULT '1',
  `is_available` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'Y',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_NICK_HOST` (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `req_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `user_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `real_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `uk_username` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `req_auth_mapping` (
  `user_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `auth_type` int(11) NOT NULL,
  UNIQUE KEY `ID_U_H_A` (`user_id`,`host_id`,`auth_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `req_issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL COMMENT '该请求的标题',
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT '该请求的描述信息',
  `query` mediumtext COLLATE utf8_unicode_ci NOT NULL COMMENT '该请求需要执行的SQL内容',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '该请求当前所处的状态',
  `create_time` int(11) DEFAULT NULL COMMENT '该请求创建（open）的时间',
  `update_time` int(11) DEFAULT NULL COMMENT '该请求更新（reopen）的时间',
  `execute_time` int(11) DEFAULT NULL COMMENT '该请求执行的时间',
  `author_id` int(11) NOT NULL COMMENT '该请求的作者ID',
  `host_id` int(11) NOT NULL COMMENT '该请求对应的数据库服务器ID',
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '该请求的类型',
  `checker_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_issue_author` (`author_id`) USING BTREE,
  KEY `FK_issue_host` (`host_id`) USING BTREE,
  KEY `idx_status_utime` (`status`,`update_time`) USING BTREE,
  KEY `idx_utime` (`update_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `req_quick_query` (
  `query_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `host_id` int(11) NOT NULL,
  `query` text NOT NULL,
  `query_time` datetime NOT NULL,
  `duration` float DEFAULT NULL,
  PRIMARY KEY (`query_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `req_session` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(200) NOT NULL DEFAULT '',
  `expire` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

```