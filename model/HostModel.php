<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:45
 */

namespace sinri\dbreq\model;


use sinri\ark\database\model\ArkDatabaseTableModel;
use sinri\ark\database\pdo\ArkPDO;

/**
 * Class HostModel
 * @package sinri\dbreq\model
 * @property integer id
 * @property string addr
 * @property integer port
 * @property string user
 * @property string pass
 * @property string nick
 * @property integer is_readonly
 * @property string is_available
 */
class HostModel extends ArkDatabaseTableModel
{
    const ACCESS_READONLY = "R";
    const ACCESS_BOTH = "RW";
    const AVAILABLE_YES = "Y";
    const AVAILABLE_NO = "N";

    /**
     * @return ArkPDO
     * @throws \Exception
     */
    public function db()
    {
        return DBReq()->db();
    }

    /**
     * @return string
     */
    protected function mappingTableName()
    {
        return "req_host";
    }

    /**
     * @param bool $availableOnly
     * @return array|bool
     */
    public function getHostList($availableOnly = false)
    {
        $conditions = [];
        if ($availableOnly) {
            $conditions['is_available'] = HostModel::AVAILABLE_YES;
        }
        $list = (new HostModel())->selectRowsWithSort($conditions, 'nick');
        if (empty($list)) $list = [];
        foreach ($list as $key => $item) {
            unset($list[$key]['pass']);
        }
        return $list;
    }
}