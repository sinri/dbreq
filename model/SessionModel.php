<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 17:01
 */

namespace sinri\dbreq\model;


use sinri\ark\database\model\ArkDatabaseTableModel;
use sinri\ark\database\pdo\ArkPDO;

/**
 * Class SessionModel
 * @package sinri\dbreq\model
 * @property integer id
 * @property integer user_id
 * @property string token
 * @property integer expire
 */
class SessionModel extends ArkDatabaseTableModel
{

    /**
     * @param $token
     * @return bool|SessionModel
     * @throws \Exception
     */
    public static function loadWithToken($token)
    {
        $model = new SessionModel();
        $sql = "SELECT * FROM " . $model->getTableExpressForSQL() . " WHERE token=? AND expire>? LIMIT 1";
        $row = $model->db()->safeQueryRow($sql, [$token, time()]);
        if (!$row) return false;
        $model->loadFieldsFromRowArray($row);
        return $model;
    }

    /**
     * @return ArkPDO
     * @throws \Exception
     */
    public function db()
    {
        return DBReq()->db();
    }

    /**
     * @return string
     */
    protected function mappingTableName()
    {
        return "req_session";
    }
}