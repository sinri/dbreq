let dbreq = {
    security: {
        getToken: () => {
            return Cookies.get('dbreq_token');
        },
        setToken: (token, expireTimestamp) => {
            console.log('expireTimestamp', expireTimestamp, new Date(expireTimestamp));
            Cookies.set('dbreq_token', token, {expires: new Date(expireTimestamp)});
        },
        cleanToken: () => {
            Cookies.remove('dbreq_token');
        },
        thisPageRequestLogin: () => {
            console.log("thisPageRequestLogin, go checking", dbreq.security.getToken());
            if (!dbreq.security.getToken()) {
                window.location.replace("login.html");
            }
        },
        logout: () => {
            dbreq.security.cleanToken();
            window.location.href = "login.html";
        }
    },
    api: {
        apiBaseUrl: "../api/",
        /**
         *
         * @param apiPath a string
         * @param data an object to package
         * @param callbackForData (data)=>{}
         * @param callbackForError (error,status)=>{}
         */
        call: (apiPath, data, callbackForData, callbackForError) => {
            if (!data) {
                data = {};
            }
            data.token = dbreq.security.getToken();
            axios.post(dbreq.api.apiBaseUrl + apiPath, data)
                .then((response) => {
                    console.log("then", response);
                    if (response.status !== 200 || !response.data) {
                        callbackForError(response.data, response.status);
                        return;
                    }
                    let body = response.data;
                    if (body.code && body.code === 'OK') {
                        console.log("success with data", body.data);
                        callbackForData(body.data);
                        return;
                    }
                    callbackForError((body.data ? body.data : 'Unknown Error'), response.status);
                })
                .catch((error) => {
                    console.log("catch", error);
                    callbackForError(error, -1);
                })
        }
    },
    page: {
        getParameterByName: function (name, defaultValue) {
            let match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
            let v = match && decodeURIComponent(match[1].replace(/\+/g, ' '));
            return v ? v : defaultValue;
        }
    }
};