Vue.component('dbreq-header', {
    props: ['menu_handler'],
    template: '<Header :style="{position: \'fixed\', width: \'100%\',\'z-index\':\'100\',margin:\'0\'}">\n' +
    '                <i-menu mode="horizontal" theme="dark" active-name="0" @on-select="onMenuSelect">\n' +
    '                    <div class="layout-logo">\n' +
    '                        <div v-on:click="onHomeLogo">DBReq<sup>©</sup></div>\n' +
    '                    </div>\n' +
    '                    <div class="layout-nav">' +
    '                       <Submenu name="issue">' +
    '                           <template slot="title">' +
    '                               <Icon type="ios-list-outline"></Icon>Issue ' +
    '                           </template>' +
    '                           <menu-group title="issue">' +
    '                               <MenuItem name="issues">Issues</MenuItem>' +
    '                               <MenuItem name="approval">Approval</MenuItem>' +
    '                               <MenuItem name="draft">Apply Draft</MenuItem>' +
    '                           </menu-group>' +
    '                        </Submenu>' +
    '                        <menu-item name="query">\n' +
    '                            <Icon type="eye"></Icon>\n' +
    '                            Query\n' +
    '                        </menu-item>\n' +
    '                        <Submenu name="admin">\n' +
    '                           <template slot="title">\n' +
    '                               <Icon type="stats-bars"></Icon>\n' +
    '                               Admin\n' +
    '                           </template>' +
    '                           <menu-group title="Management">\n' +
    '                               <MenuItem name="databases">Manage Databases</MenuItem>\n' +
    '                               <MenuItem name="users">Manage Users</MenuItem>\n' +
    '                               <MenuItem name="permitting">Manage Permission</MenuItem>\n' +
    '                           </menu-group>\n' +
    '                        </Submenu>\n' +
    '                        <menu-item name="logout">\n' +
    '                            <Icon type="log-out"></Icon>\n' +
    '                            Logout\n' +
    '                        </menu-item>\n' +
    '                    </div>\n' +
    '                </i-menu>\n' +
    '            </Header>',
    methods: {
        onHomeLogo: function () {
            window.location.href = "index.html";
        },
        onMenuSelect: function (menu_name) {
            switch (menu_name) {
                case 'logout':
                    dbreq.security.logout();
                    break;
                case 'issues':
                    window.location.href = "issues.html";
                    break;
                case 'approval':
                    window.location.href = "approval.html";
                    break;
                case 'draft':
                    window.location.href = "draft.html";
                    break;
                case 'query':
                    window.location.href = "query.html";
                    break;
                case 'databases':
                    window.location.href = "databases.html";
                    //this.$emit('open_menu_item',menu_name);
                    break;
                case 'users':
                    window.location.href = "users.html";
                    //this.$emit('open_menu_item',menu_name);
                    break;
                case 'permitting':
                    window.location.href = 'permitting.html';
                    break;
                default:
                //do nothing
            }
        }
    },
    mounted: function () {
        dbreq.security.thisPageRequestLogin();
    }
});