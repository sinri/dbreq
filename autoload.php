<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 13:58
 */

require_once __DIR__ . '/vendor/autoload.php';
\sinri\ark\core\ArkHelper::registerAutoload(
    "sinri\dbreq",
    __DIR__,
    ".php"
);

if (!function_exists('DBReq')) {
    function DBReq()
    {
        return \sinri\dbreq\core\DBReqCore::getInstance();
    }
}