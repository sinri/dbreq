<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 22:53
 */

namespace sinri\dbreq\plugin\standard;


use sinri\ark\core\ArkHelper;
use sinri\dbreq\entity\SessionEntity;
use sinri\dbreq\entity\UserEntity;
use sinri\dbreq\model\UserModel;
use sinri\dbreq\plugin\interfaces\UserPlugin;

class StandardUserPlugin extends UserPlugin
{


    /**
     * @return SessionEntity
     * @throws \Exception
     */
    public function createSession()
    {
        $username = Ark()->webInput()->readRequest("username");
        $password = Ark()->webInput()->readRequest("password");

        $user = UserEntity::buildInstanceWithUsername($username);
        ArkHelper::assertItem($user, "Cannot Construct User");

        if (!password_verify($password, $user->getPasswordHash())) {
            throw new \Exception("Cannot Verify User Permission");
        }

        $session = SessionEntity::createSessionForUser($user);
        ArkHelper::assertItem($session, "Cannot Create Session");
        return $session;
    }

    /**
     * @throws \Exception
     */
    public function updateUser()
    {
        if (!DBReq()->getCurrentSession()->getUser()->isAdmin()) {
            throw new \Exception("Admin Only Knows");
        }

        $user_id = Ark()->webInput()->readRequest("user_id", null, '/^[1-9][0-9]*$/');
        $is_admin = Ark()->webInput()->readRequest("is_admin", 0);
        $user_name = Ark()->webInput()->readRequest("user_name");
        $real_name = Ark()->webInput()->readRequest("real_name");
        $password = Ark()->webInput()->readRequest("password", null, '/^.+$/');

        $is_admin = (intval($is_admin, 10) !== 0 ? 1 : 0);

        if ($user_id === null) {
            // create new user, here request user_id is AI
            ArkHelper::assertItem($user_name, 'Lack of User Name');
            ArkHelper::assertItem($real_name, 'Lack of Real Name');
            ArkHelper::assertItem($password, 'Lack of Password');

            $user_id = (new UserModel())->insert([
                'is_admin' => $is_admin,
                'user_name' => $user_name,
                'real_name' => $real_name,
                'password' => password_hash($password, PASSWORD_DEFAULT),
            ]);
            ArkHelper::assertItem($user_id, 'Cannot Create User Info');
        } else {
            // update existed user
            $data = ['is_admin' => $is_admin];
            if ($user_name !== null) $data['user_name'] = $user_name;
            if ($real_name !== null) $data['real_name'] = $real_name;
            if ($password !== null) $data['password'] = password_hash($password, PASSWORD_DEFAULT);
            $afx = (new UserModel())->update(
                ['user_id' => $user_id],
                $data
            );
            ArkHelper::assertItem($afx, 'Cannot Update User Info');
        }
    }
}