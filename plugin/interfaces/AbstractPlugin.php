<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 22:57
 */

namespace sinri\dbreq\plugin\interfaces;


abstract class AbstractPlugin
{
    public static function loadInstance()
    {
        $pluginImplementationName = DBReq()->readConfig(['plugin', static::pluginCategory()]);
        if (!$pluginImplementationName) {
            $pluginClassName = "sinri\\dbreq\\plugin\\standard\\Standard" . static::pluginCategory();
        } else {
            $pluginClassName = "sinri\\dbreq\\plugin\\thirdparty\\" . $pluginImplementationName . static::pluginCategory();
        }
        return new $pluginClassName();
    }

    abstract public static function pluginCategory(): string;
}