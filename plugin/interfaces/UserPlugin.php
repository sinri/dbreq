<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 22:37
 */

namespace sinri\dbreq\plugin\interfaces;


use sinri\dbreq\entity\SessionEntity;

abstract class UserPlugin extends AbstractPlugin
{
    /**
     * @return string
     */
    public final static function pluginCategory(): string
    {
        return "UserPlugin";
    }

    /**
     * @return UserPlugin
     */
    public final static function loadInstance()
    {
        return parent::loadInstance();
    }

    /**
     * As Controller Method
     * If success: return data for OK response
     * Else: throw an Exception for FAIL response
     * @return SessionEntity
     * @throws \Exception
     */
    abstract public function createSession();

    /**
     * As Controller Method
     * If success: return data for OK response
     * Else: throw an Exception for FAIL response
     * @throws \Exception
     */
    abstract public function updateUser();
}