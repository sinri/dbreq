<?php
/**
 * Created by PhpStorm.
 * User: Sinri
 * Date: 2018/3/8
 * Time: 16:08
 */

$config['database'] = [
    'host' => 'mysql.example.com',
    'port' => 3306,
    'username' => '',
    'password' => '',
    'database' => 'dbreq',
];

$config['storage_dir'] = __DIR__ . '/../runtime/storage';
$config['log_dir'] = __DIR__ . '/../runtime/log';
$config['log_level'] = \Psr\Log\LogLevel::INFO;

$config['session_life'] = 3600;

$config['plugin'] = [
    //'UserPlugin'=>'LeqeeAA',// no such key or value is null, use standard
];